#include "BS86D12C.h"
#include <stdint.h>

void __attribute((interrupt(0x28))) UART_ISR() {
	static uint8_t byte = 0;
	if(!_tidle0) {
		return;	
	}
	if(_txif0 || _ur0f) {
		byte = _u0sr;
		_txr_rxr0 = 'h';
	}
}

void delay(uint16_t delay) {
	
	volatile uint16_t i = delay;
	while(delay) { delay--; }
}

void main()
{
	static uint8_t byte = 0;
	_ws0 = 1;
	_ws1 = 1;
	_ws2 = 1;
	// io
	_pac3 = 0;
	_pac1 = 1;
	//_tx0ps1 = 1;
	//  uart
	_brg0 = 0x1A;
	_tiie0 = 1;
	_txen0 = 1;
	_ur0e = 1;
	_stops0 = 1;
	_teie0 = 1;
	//_uarten0 = 1;
	//  interrupts
//	_ur0e = 1;
//	_inte = 1;
//	_emi = 1;
	// start transmission
//	byte = _u0sr;
//	_txr_rxr0 = 'h';
	while(1) {
		_clrwdt();
		_clrwdt1();
		_clrwdt2();
		_pc2 = 1;
		delay(1000);
		_pc2 = 0;
		delay(1000);
	}
}

