#include "BS86D12C.h"
#include <stdint.h>

#define fH			(unsigned long)			8000000
#define BRate		(unsigned long)			9600
#define N_Baud		(fH/(BRate*64)-1)

const char *hello = "Hello world";
const unsigned char hello_len = 11;

void UART_Init(void)
{
	_pac3 = 0;
	_pac1 = 1;

	_rx0ps1 = 0;
	_rx0ps0 = 1;
	_rx0en = 1;
	 
	_tx0ps1 = 0;
	_tx0ps0 = 1;
	
	_u0cr1 = 0b10000000;
	_u0cr2 = 0b11000100;
	
	_brg0 = N_Baud;
	
	_teie0 = 1;
	_tiie0 = 1;
	_rie0 = 1;
	
	_ur0f = 0;
	_ur0e = 1;

	_inte = 1;
	_emi = 1;
}

void UART_Tx()
{
	int i;
	while(_tidle0 == 0 || _txif0 == 0);
	for(i=0; i< hello_len; i++){
		_txr_rxr0 = hello[i];
		while(_tidle0 == 0 || _txif0 == 0);
	}
}



DEFINE_ISR (Interrupt_Uart, 0x28)
{
	static unsigned char tx_cnt = 0;
	if(_txif0 && _tidle0) {
		_txr_rxr0 = hello[tx_cnt++];
		if (tx_cnt >= hello_len)
			tx_cnt = 0;
	}
	if(_rxif0)
	{	
		
	}
	_clrwdt();
	_clrwdt1(); 
	_clrwdt2();
}

void main(void) {
	UART_Init();
	while(1);
}
